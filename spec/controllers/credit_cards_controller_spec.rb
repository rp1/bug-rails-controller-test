require 'rails_helper'

RSpec.describe CreditCardsController, type: :controller do

  describe "GET #open" do
    it "returns http success" do
      request.headers["HTTP_REFERER"] = "https://example.com/home"
      get :open
      expect(response).to have_http_status(:success)
    end
  end

end
