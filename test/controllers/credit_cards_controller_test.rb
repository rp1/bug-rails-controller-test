require 'test_helper'

class CreditCardsControllerTest < ActionDispatch::IntegrationTest
  test "should get open" do
    get credit_cards_open_url, headers: { "HTTP_REFERER": "https://example.com/home" }
    assert_response :success
  end

  test "should get open failure" do
    get credit_cards_open_url
    assert_response :bad_request
  end

end
