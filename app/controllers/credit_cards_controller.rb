class CreditCardsController < ApplicationController
  def open
    head(:bad_request) unless request.referer == 'https://example.com/home'
  end
end
